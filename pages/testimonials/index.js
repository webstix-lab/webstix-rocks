import { GraphQLClient } from "graphql-request";
import Layout from '../../components/Layout'
import Sidebar from '../../components/Sidebar'
import Link from "next/link";
import ReactPlayer from 'react-player';

const graphcms = new GraphQLClient(process.env.GRAPHQL_URL_ENDPOINT);

export async function getStaticProps() {
  const { video_testimonials } = await graphcms.request(
    `
    {
      video_testimonials{
      id
      title
      slug
      videourl
      description 
    }
  }
  `
  );

  return {
    props: {
      video_testimonials,
    },
  };
}

export default ({ video_testimonials }) => {
  return (
    <div>
      <Layout >
      
      <section class="bg-black">
        <div class="container my-0 mx-auto bg-white max-w-7xl">
          <div class="grid grid-cols-3 gap-4">
            <div class="col-span-2 pr-5 py-8 px-8"> 
            {video_testimonials.map((video) => {
        return (        
              <div class="border-b-2 border-gray-100 pb-8 mb-6">
                <h1 class="text-3xl font-sans hover:text-red-800 cursor-pointer"><Link key={video.id}   as={`/testimonials/${video.slug}`}  href="/testimonials/[slug]">{video.title}</Link></h1>
                <div class="my-5">
                <ReactPlayer url={video.videourl} />
               

                </div>
                <p>{video.description}</p>
                </div>
                );
              })}
            </div>
            <Sidebar/>
          </div>
        
        </div>
      </section>  
     </Layout>
    </div>
  );
};


import { GraphQLClient } from "graphql-request";
import Link from "next/link";
import Layout from '../../components/Layout'
import Sidebar from '../../components/Sidebar'
import ReactPlayer from "react-player";

const graphcms = new GraphQLClient(process.env.GRAPHQL_URL_ENDPOINT);

export async function getStaticProps({ params }) {
  const { videos } = await graphcms.request(
    `
    query videos($slug: String!) {
      videos(where: { slug: $slug }) {
        id
        title
        slug
        videourl
        description 
      }
    }
  `,
    {
      slug: params.slug,
    }
  );

  return {
    props: {
      videos,
    },
  };
}

export async function getStaticPaths() {
  const { video_testimonials } = await graphcms.request(
    `
  {
    video_testimonials{
    id
    title
    slug
    videourl
    description 
  }
}
  `);

  return {
    paths: video_testimonials.map(({ slug }) => ({
      params: { slug },
    })),
    fallback: false,
  };
}

export default ({ videos }) => {
  return (
    <div>
    <Layout title={videos.title}>

    <section class="bg-black">
      <div class="container my-0 mx-auto bg-white max-w-7xl">
        <div class="grid grid-cols-3 gap-4">
          <div class="col-span-2 pr-5 py-8 px-8">
            <div class="border-b-2 border-gray-100 pb-8 mb-6">
              <h1 class="text-3xl font-sans hover:text-red-800 cursor-pointer">{videos.title}</h1>
              <div class="my-5"> <ReactPlayer url={videos.videourl} /></div>
              <p>{videos.description}</p>
            </div>
          
          </div>
          <Sidebar/>
        </div>
      
      </div>
    </section>     
   </Layout>
  </div>
  );
};


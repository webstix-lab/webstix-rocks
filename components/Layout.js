import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Header from './Header'
import Footer from './Footer'


export default function Layout({title, description, keywords, children}) {
    return (
        <div>
        <Head>
            <title>{title}</title>
            <meta name="description" content={description}/>
            <meta name="keywords" content={keywords}/>
            <link rel="icon" href="../index.ico" />
        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet"></link>
            
        </Head>
        <Header/>
       {children}
       <Footer/>
        </div>
    )
}

Layout.defaultProps={
    title:'Webstix Rocks | Video Testimonials',
    description:'Clients talks about Webstix',
    keywords: 'webstix, videos, rocks'
}
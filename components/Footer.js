import Link from 'next/link'

export default function Footer() {
    return (
        <footer class="bg-gray-500 border-t-4 border-red-600 py-2">
        <p class="text-left container mx-auto max-w-7xl text-gray-300"><span class="text-red-600">Webstix Rocks!</span> Copyright © 2022.</p>
      </footer>
    )
}

import Link from 'next/link'
export default function Sidebar() {
    return (
        <div class="bg-gray-100 border-l py-8 px-8">
        <div class="bg-gray-900 py-4 px-6">
          <p class="text-white">You can also say</p>
          <h2 class="text-red-600 text-3xl pt-2">"Webstix Rocks!"</h2>
          <p class="text-white pt-2">when we complete a website design project for you.</p>
          <p class="text-gray-400 text-xl pt-2 mb-3">Contact Webstix Today <span class="bg-green-700 text-white py-2 px-3 cursor-pointer hover:bg-red-600 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110">CLICK!</span></p>
        </div>
        <div class="bg-black text-white text-xl py-2 pl-3 hover:text-red-500 cursor-pointer">#WebstixRocks</div>
        <div class="flex my-5 text-gray-500 hover:text-red-500 cursor-pointer"><Link href="/testimonials"><a>See all Testimonials <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 hover:text-red-500 cursor-pointer text-gray-500 pt-1 mx-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 8l4 4m0 0l-4 4m4-4H3" />
        </svg></a></Link></div>
        <h3 class="text-gray-500 text-2xl">Webstix</h3>
        <p class="text-gray-500 pt-4 break-normal">
          2820 Walton Commons Ln<br/>
          Suite 108<br/>
          Madison, WI 53718<br/>
          www.webstix.com
        </p>
      </div>
    )
}

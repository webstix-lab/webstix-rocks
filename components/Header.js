import Link from 'next/link'
import styles from '../styles/Home.module.css'
export default function Header() {
    return (
        <header class="py-4 bg-black bg-local" >
        <div class="container mx-auto px-8 max-w-7xl">
          <div class="flex flex-wrap">
            <div class="font-bold text-4xl flex-1"><Link href='/'><a><img src="/logo1.png"></img></a></Link></div>
            <div class="space-x-4 my-8"><img class="inline-block" src="/graphic_btn1.png"></img><img class="inline-block" src="/graphic_btn2.png"></img><img class="inline-block" src="/graphic_btn3.png"></img><img class="inline-block" src="/graphic_btn4.png"></img><img class="inline-block" src="/graphic_btn5.png"></img><img class="inline-block" src="/on_off_btn.png"></img></div>
          </div>
        </div>
      </header>

    )
}
